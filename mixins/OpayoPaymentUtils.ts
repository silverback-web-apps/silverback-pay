import {MerchantSessionKeysResponse, PaymentPostData} from '~/utils/opayo-utils';
import {Contact, Invoice as InvoiceType} from 'xero-node';

interface TokenisedResult {
    success: boolean
    cardIdentifier: string
}

type onTokenisedFn = (result: TokenisedResult) => void;

export interface CardDetailsInterface {
    cardholderName: string
    cardNumber: string
    expiryDate: string
    securityCode: string
}

export interface BillingAddressInterface {
    firstName: string
    lastName: string
    address1: string
    address2: string
    address3: string
    city: string
    postalCode: string
    country: string
    region: string
}

export interface BasketInterface {
    selectedInvoices: InvoiceType[]
    total: number
    contact: Contact
}

interface tokeniseCardDetailsConfig {
    cardDetails: CardDetailsInterface
    onTokenised: onTokenisedFn
}

type tokeniseCardDetailsFn = (data: tokeniseCardDetailsConfig) => void;

interface sagePayOwnFormInstance {
    tokeniseCardDetails: tokeniseCardDetailsFn
}

declare function sagepayOwnForm(
    {
        merchantSessionKey: string
    }
): sagePayOwnFormInstance;

export default function OpayoPaymentUtils() {
    const getMerchantSessionKey = async () => {
        try {
            const { merchantSessionKey }: MerchantSessionKeysResponse = await $fetch('/opayo/merchant-session-keys')
            return merchantSessionKey
        } catch(error) {
            console.error(error)
            throw error
        }
    }

    const getCardIdentifier = (merchantSessionKey: string, cardDetails: CardDetailsInterface) => {
        return new Promise<string>( (resolve, reject) => {
            const onTokenised = (result) => {
                if (result.success) {
                    resolve(result.cardIdentifier);
                } else {
                    reject(result)
                }
            }
            sagepayOwnForm({
                merchantSessionKey
            }).tokeniseCardDetails({
                cardDetails: {
                    ...cardDetails,
                    cardNumber: cardDetails.cardNumber.replace(/\s/g, '')
                },
                onTokenised
            });
        })
    }

    const postPayment = async (merchantSessionKey: string, cardIdentifier: string, address: BillingAddressInterface, basket: BasketInterface): Promise<any> => {
        const date = new Date();
        const browserTZ = date.getTimezoneOffset();
        let browserColorDepth = window.screen.colorDepth
        // Chrome reports 30 with deep color support - invalid for 3ds
        // valid values are 1,4,8,15,16,24,32,48
        if (browserColorDepth === 30) {
            browserColorDepth = 24
        }
        const body:PaymentPostData = {
            merchantSessionKey,
            cardIdentifier,
            address,
            basket,
            browser: {
                browserJavascriptEnabled: true,
                browserJavaEnabled: navigator.javaEnabled(),
                browserLanguage: navigator.language,
                browserColorDepth,
                browserScreenHeight: `${window.screen.height}`,
                browserScreenWidth: `${window.screen.width}`,
                browserTZ: browserTZ > 0 ? `+${browserTZ}` : `${browserTZ}`,
                browserUserAgent: navigator.userAgent
            }
        }

        try {
            return await $fetch('/opayo/payment', {
                method: 'POST',
                body
            })
        } catch(error) {
            console.error(error)
            throw error
        }
    }

    const xeroComplete = async (transactionId: string, sc: string) => {
        try {
            return await $fetch('/xero/complete', {
                method: 'POST',
                body: {
                    transactionId,
                    sc
                }
            })
        } catch(error) {
            console.error(error)
            throw error
        }
    }

    return { getMerchantSessionKey, getCardIdentifier, postPayment, xeroComplete }
}
