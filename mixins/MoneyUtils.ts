export const invoiceFeeBreakpoint = 250
export const invoiceFeePercent = 0.02

export function formatMoney(amount: number) {
    return (Math.round(amount * 100) / 100).toFixed(2)
}

export function calculateFees(invoiceTotal: number) {
    if (invoiceTotal > invoiceFeeBreakpoint) {
        const fee = invoiceTotal * invoiceFeePercent
        return Math.round(fee * 100) / 100;
    }
    return 0
}
