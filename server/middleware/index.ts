import ServerStorage from "~/utils/server-storage";
import XeroUtils from '~/utils/xero-utils';
import OpayoUtils from '~/utils/opayo-utils';
import { url as ngrokUrl } from '#build/ngrok.mjs'
import {CompatibilityEvent} from 'h3';
import ipware from 'ipware'

const runtimeConfig = useRuntimeConfig()

export default defineEventHandler((event: CompatibilityEvent) => {
  const req = event.req
  const res = event.res

  const getIP = ipware().get_ip
  const { clientIp } = getIP(event.req);
  event.context.clientIp = clientIp

  req.storage = new ServerStorage();
  event.context.HOST = runtimeConfig.public.APP_HOST || ngrokUrl || "http://localhost"

  const isXeroEndpoint = req.url.includes("/xero/");
  if (isXeroEndpoint) {
    req.xero = new XeroUtils(event);
    // we need to get up to date invoices
    res.setHeader(
        "Cache-Control",
        "public, max-age=3, s-max-age=3, stale-while-revalidate=0"
    );
  }
  const isOpayoEndpoint = req.url.includes("/opayo/");
  if (isOpayoEndpoint) {
    req.opayo = new OpayoUtils(event);
    res.setHeader(
        "Cache-Control",
        "public, max-age=0, s-max-age=0, stale-while-revalidate=0"
    );
  }
})
