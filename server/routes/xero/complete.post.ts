export default defineEventHandler(async (event) => {
    await event.req.xero.validateXeroRequest()
    if (event.res.writableEnded) {
        return
    }

    return await event.req.xero.paymentComplete()
})
