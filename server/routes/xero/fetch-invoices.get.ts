export default defineEventHandler(async (event) => {
    const request = event.req
    const response = event.res
    await request.xero.validateXeroRequest()
    if (response.writableEnded) {
        return
    }

    const invoices = await request.xero.fetchInvoices()
    if (!invoices.success && response.statusCode === 200) {
        response.statusCode = 500
    }
    return invoices
})
