export default defineEventHandler(async (event) => {
    const request = event.req
    const response = event.res

    await request.xero.validateXeroRequest()
    if (response.writableEnded) {
        return
    }
    const result = await request.xero.getOnlineInvoiceUrl(request, response)
    if (!result.success) {
        response.statusCode = 500
        return result
    }
    response.writeHead(302, {
        Location: result.message
    });
    response.end()
})
