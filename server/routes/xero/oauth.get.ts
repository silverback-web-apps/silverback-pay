export default defineEventHandler(async (event) => {
    event.res.writeHead(302, {
        Location: await event.req.xero.getOauthConsentUrl()
    });
    event.res.end()
})
