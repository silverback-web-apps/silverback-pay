import {useQuery} from 'h3';

export default defineEventHandler(async (event) => {
    const request = event.req
    const response = event.res
    const tokenSet = await request.xero.apiCallback();
    if (!tokenSet) {
        response.statusCode = 500;
        response.write('Sorry, an unexpected error occurred. Please check the server logs.');
        response.end();
        return
    }
    const { returnUrl } = useQuery(event)
    response.writeHead(302, {
        Location: returnUrl || '/xero/oauth-success'
    });
    response.end()
})
