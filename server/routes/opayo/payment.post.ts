export default defineEventHandler(async (event) => {
    return await event.req.opayo.createPayment()
})
