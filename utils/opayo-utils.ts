import {CompatibilityEvent, useBody} from 'h3';
import {BasketInterface, BillingAddressInterface} from '~/mixins/OpayoPaymentUtils';
import { v4 as uuidv4 } from 'uuid';
import {FetchError} from 'ohmyfetch';

const { OPAYO_INTEGRATION_KEY, OPAYO_INTEGRATION_PASSWORD, OPAYO_VENDOR, public: { OPAYO_HOST } } = useRuntimeConfig()

export interface MerchantSessionKeysResponse {
    merchantSessionKey: string
    expiry: string
}

export interface PaymentPostData {
    basket: BasketInterface
    address: BillingAddressInterface
    merchantSessionKey: string
    cardIdentifier: string
    browser: BrowserSpecs
}

interface BrowserSpecs {
    browserJavascriptEnabled: boolean;
    browserJavaEnabled: boolean;
    browserLanguage: string;
    browserColorDepth: number;
    browserScreenHeight: string;
    browserScreenWidth: string;
    browserTZ: string;
    browserUserAgent: string;
}

declare module OpayoPostData {
    export interface Card {
        merchantSessionKey: string;
        cardIdentifier: string;
    }

    export interface PaymentMethod {
        card: Card;
    }

    export interface BillingAddress {
        address1: string;
        address2?: string;
        address3?: string;
        city: string;
        postalCode: string;
        country: string;
        state?: string
    }

    export interface StrongCustomerAuthentication extends BrowserSpecs{
        browserIP: string;
        browserAcceptHeader: string;
        website: string;
        notificationURL: string;
        challengeWindowSize: string;
        threeDSRequestorChallengeInd: string;
        requestSCAExemption: boolean;
        transType: string;
        threeDSRequestorDecReqInd: string;
    }

    export interface RootObject {
        transactionType: string;
        paymentMethod: PaymentMethod;
        vendorTxCode: string;
        amount: number;
        currency: string;
        description: string;
        apply3DSecure: string;
        applyAvsCvcCheck: string;
        customerFirstName: string;
        customerLastName: string;
        customerEmail: string;
        customerPhone: string;
        billingAddress: BillingAddress;
        entryMethod: string;
        strongCustomerAuthentication: StrongCustomerAuthentication;
    }
}

export interface TransactionStoreItem {
    paymentPostData: PaymentPostData
    response?: any
    threeDSResponse?: any
    notificationURL: string
}

export default class OpayoUtils {
    private readonly headers: { Authorization: string,  'Content-type': string };
    private event: CompatibilityEvent;

    constructor(event: CompatibilityEvent) {
        this.event = event
        const authString = `${OPAYO_INTEGRATION_KEY}:${OPAYO_INTEGRATION_PASSWORD}`
        const base64Auth = Buffer.from(authString).toString('base64')
        this.headers = {
            Authorization: `Basic ${base64Auth}`,
            'Content-type': 'application/json'
        }
    }

    public async getMerchantSessionKey(): Promise<MerchantSessionKeysResponse>
    {
        const endpoint = `${OPAYO_HOST}/merchant-session-keys`
        const requestObj = {
            method: 'POST',
            headers: this.headers,
            body: {
                vendorName: OPAYO_VENDOR
            }
        }
        return await $fetch(endpoint, requestObj)
    }

    public async createPayment()
    {
        const paymentPostData:PaymentPostData = await useBody(this.event)
        // 36 chars of max 40
        const vendorTxCode = uuidv4()
        const notificationURL = `${this.event.context.HOST}/opayo/notification`
        const storageItem: TransactionStoreItem = {
            paymentPostData,
            response: null,
            threeDSResponse: null,
            notificationURL
        }
        this.event.req.storage.storage.setItem(vendorTxCode, storageItem)
        const selectedInvoicesStr = paymentPostData.basket.selectedInvoices.map(invoice => invoice.invoiceID).join(', ')

        const phoneNumber = paymentPostData.basket.contact.phones[0]
        let customerPhone
        if (!phoneNumber) {
            customerPhone = ''
        } else {
            customerPhone = `${phoneNumber.phoneCountryCode}${phoneNumber.phoneAreaCode}${phoneNumber.phoneNumber}`?.substring(0, 19)
        }
        const opayoPostData: OpayoPostData.RootObject = {
            transactionType: 'Payment',
            paymentMethod: {
                card: {
                    merchantSessionKey: paymentPostData.merchantSessionKey,
                    cardIdentifier: paymentPostData.cardIdentifier
                }
            },
            vendorTxCode,
            amount: paymentPostData.basket.total * 100,
            currency: 'GBP',
            description: selectedInvoicesStr?.substring(0, 100) || '',
            customerFirstName: paymentPostData.address.firstName?.substring(0, 20) || '',
            customerLastName: paymentPostData.address.lastName?.substring(0, 20) || '',
            customerEmail: paymentPostData.basket.contact.emailAddress?.substring(0, 90) || '',
            customerPhone,
            billingAddress: {
                address1: paymentPostData.address.address1?.substring(0,50) || '',
                address2: paymentPostData.address.address2?.substring(0,50) || '',
                address3: paymentPostData.address.address3?.substring(0,50) || '',
                city: paymentPostData.address.city?.substring(0,40) || '',
                postalCode: paymentPostData.address.postalCode?.substring(0,10) || '',
                country: paymentPostData.address.country?.substring(0,2) || '',
                state: paymentPostData.address.region?.substring(0,2) || '',
            },
            entryMethod: 'Ecommerce',
            apply3DSecure: "UseMSPSetting",
            applyAvsCvcCheck: "UseMSPSetting",
            strongCustomerAuthentication: {
                ...paymentPostData.browser,
                website: this.event.context.HOST,
                notificationURL,
                browserIP: this.event.context.clientIp || '127.0.0.1',
                browserAcceptHeader: this.event.req.headers.accept,
                challengeWindowSize: 'Small',
                threeDSRequestorChallengeInd: "02",
                requestSCAExemption: false,
                transType: "GoodsAndServicePurchase",
                threeDSRequestorDecReqInd: "N"
            }
        }

        const endpoint = `${OPAYO_HOST}/transactions`
        const requestObj = {
            method: 'POST',
            headers: this.headers,
            body: opayoPostData
        }

        let response
        try {
            response = await $fetch(endpoint, requestObj)
        } catch (error) {
            if ((error instanceof FetchError)) {
                this.event.res.statusCode = error.response.status
                return error.data
            } else {
                throw error
            }
        }

        storageItem.response = response
        this.event.req.storage.storage.setItem(response.transactionId, storageItem)
        this.event.req.storage.storage.setItem(vendorTxCode, { transactionId: response.transactionId })

        response.notificationURL = notificationURL
        return response
    }

    async handle3ds() {
        const body = await useBody(this.event)

        const cres = body.cres
        const transactionId = body.threeDSSessionData

        // @ts-ignore
        const storageItem: TransactionStoreItem = await this.event.req.storage.storage.getItem(transactionId)

        let response
        const endpoint = `${OPAYO_HOST}/transactions/${transactionId}/3d-secure-challenge`
        try {
            response = await $fetch(endpoint, {
                method: 'POST',
                body: { cRes: cres },
                headers: this.headers
            })
        } catch (error) {
            if ((error instanceof FetchError)) {
                this.event.res.statusCode = error.response.status
                return error.data
            } else {
                throw error
            }
        }

        const publishTarget = process.env.NODE_ENV === 'development' ? '*' : this.event.context.HOST
        storageItem.threeDSResponse = response
        await this.event.req.storage.storage.setItem(transactionId, storageItem)

        this.event.res.write(`
        <html>
        <body>  
        Redirecting you now...
        <script type="application/javascript">
        window.parent.postMessage(${JSON.stringify(response)}, "${publishTarget}")
        </script>
        </body>
        </html>
        `)
        this.event.res.end()

        return true
    }
}
