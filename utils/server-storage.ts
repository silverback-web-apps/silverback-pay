import { createStorage, Storage } from "unstorage";
// @ts-ignore
import redisDriver from "unstorage/drivers/redis";
import { SessionInterface } from "@shopify/shopify-api";
import { Session } from "@shopify/shopify-api/dist/auth/session/index.js";

const { REDIS_URL } = useRuntimeConfig()

export default class ServerStorage {
  storage: Storage = null;
  constructor() {
    this.storage = createStorage({
      driver: redisDriver({ base: "storage:", url: REDIS_URL }),
    });
  }

  storeCallback = async (session: Session) => {
    try {
      // Inside our try, we use the `setAsync` method to save our session.
      // This method returns a boolean (true is successful, false if not)
      await this.storage.setItem(session.id, JSON.stringify(session));
      return true;
    } catch (err) {
      throw new Error(err);
    }
  };
  loadCallback = async (id: string): Promise<SessionInterface> => {
    const session = await this.storage.getItem(id);
    if (session instanceof Session) {
      return session;
    }
    await this.storage.removeItem(id);
    return null;
  };
  deleteCallback = async (id: string) => {
    try {
      // Inside our try, we use the `setAsync` method to save our session.
      // This method returns a boolean (true is successful, false if not)
      await this.storage.removeItem(id);
      return true;
    } catch (err) {
      throw new Error(err);
    }
  };
}
