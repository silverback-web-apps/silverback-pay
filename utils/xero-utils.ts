import {Invoice, Invoices, LineAmountTypes, Payment, TokenSet, TokenSetParameters, XeroClient} from 'xero-node';
import {IncomingMessage, ServerResponse} from 'http';
import {NuxtApp} from '@nuxt/schema';
import ServerStorage from '~/utils/server-storage';
import {URL, URLSearchParams} from 'url';
import {CompatibilityEvent} from 'h3';
import { TransactionStoreItem} from '~/utils/opayo-utils';
import {calculateFees} from '~/mixins/MoneyUtils';
import TypeEnum = Invoice.TypeEnum;
import StatusEnum = Invoice.StatusEnum;
import {FetchError} from 'ohmyfetch';

const runtimeConfig = useRuntimeConfig()

export default class XeroUtils {
    private readonly request: IncomingMessage;
    private readonly response: ServerResponse;
    private readonly nuxtApp: NuxtApp;
    private xero: XeroClient;
    private sessionStorage: ServerStorage;
    private tokenSet: TokenSet;
    private readonly event: CompatibilityEvent;

    constructor(event: CompatibilityEvent) {
        this.event = event
        const request = this.event.req
        const response = this.event.res

        this.sessionStorage = request.storage;
        this.request = request;
        this.response = response;
        this.nuxtApp = request.nuxtApp;

        // const { returnUrl } = useQuery(this.event)

        this.xero = new XeroClient({
            clientId: runtimeConfig.XERO_CLIENT_ID,
            clientSecret: runtimeConfig.XERO_CLIENT_SECRET,
            redirectUris: [`${event.context.HOST}/xero/oauth-redirect`], // ?returnUrl=${returnUrl} - unfortunately this results in invalid redirect URL, no querystring allowed. Would need to cache and redirect on return for the session
            scopes: runtimeConfig.XERO_SCOPES.split(" "),
            // state: 'returnPage=my-sweet-dashboard', // custom params (optional)
            // httpTimeout: 3000 // ms (optional)
        });
    }

    async isXeroTokenValid()
    {
        await this.xero.initialize();

        const hasToken = await this.sessionStorage.storage.hasItem('xeroToken');
        if (!hasToken) {
            return false
        }
        const tokenSetString = await this.sessionStorage.storage.getItem('xeroToken')
        // @ts-ignore
        const tokenSetParameters: TokenSetParameters = tokenSetString.valueOf()
        this.tokenSet = new TokenSet(tokenSetParameters)
        await this.xero.setTokenSet(this.tokenSet);
        if(!this.tokenSet.expired()){
            return true
        }
        const newTokenSet = await this.xero.refreshToken();
        await this.saveTokenSet(newTokenSet)
        return true
    }

    async getOauthConsentUrl()
    {
        return await this.xero.buildConsentUrl()
    }

    async apiCallback(): Promise<TokenSet>
    {
        try {
            const tokenSet: TokenSet = await this.xero.apiCallback(this.event.req.url);
            await this.saveTokenSet(tokenSet)
            return tokenSet
        } catch (error) {
            console.error(error)
            return null
        }
    }

    async saveTokenSet(tokenSet: TokenSet) {
        await this.sessionStorage.storage.setItem('xeroToken', tokenSet);
    }

    async deleteXeroToken()
    {
        await this.sessionStorage.storage.removeItem('xeroToken');
    }

    async validateXeroRequest()
    {
        if (!(await this.isXeroTokenValid())) {
            this.response.statusCode = 403
            this.response.write('Unauthorized');
            this.response.end();
            return false
        }
        return true
    }

    // should migrate to useQuery
    private static getParams(request: IncomingMessage, requiredParams: string[]): any
    {
        const requestUrl = new URL(`https://${request.headers.host}${request.url}`);
        const queryParams: URLSearchParams = requestUrl.searchParams;

        const params = {}
        for (const param of requiredParams) {
            params[param] = null
        }
        for(const param of requiredParams) {
            if (!queryParams.has(param)) {
                continue
            }
            params[param] = queryParams.get(param)
        }
        return params
    }

    async fetchInvoices()
    {
        // should migrate to useQuery
        const params = XeroUtils.getParams(this.request, ['sc', 'inv', 'am', 'c'])

        let tenant
        let invoice
        let contact
        let additionalInvoices
        let bankAccounts
        try {
            tenant = await this.fetchTenant(params.sc)
            invoice = await this.fetchInvoice(tenant.tenantId, params.inv)
            if (invoice.amountDue / 1 !== params.am / 1) {
                this.event.res.statusCode = 404
                return XeroUtils.buildResponse(false, 'The invoice could not be found. Perhaps you are not using the most recent invoice link.')
            }

            contact = invoice.contact
            additionalInvoices = await this.fetchInvoicesByContact(tenant.tenantId, contact.contactID)
            bankAccounts = await this.fetchBankAccounts(tenant.tenantId)
        } catch(error) {
            return this.normalizeErrorResponse(error)
        }

        const invoices = [invoice, ...additionalInvoices.filter(({ invoiceNumber }) => (invoiceNumber !== params.inv) )].map(invoice => XeroUtils.stripInvoiceKeys(invoice))

        return XeroUtils.buildResponse(
            true,
            null,
            {
                contact,
                invoices,
                bankAccounts
            })
    }

    async getOnlineInvoiceUrl(request: IncomingMessage, response: ServerResponse)
    {
        // should migrate to useQuery
        const params = XeroUtils.getParams(request, ['sc', 'inv'])

        let tenant
        let invoiceUrl
        try {
            tenant = await this.fetchTenant(params.sc)
            invoiceUrl = await this.fetchOnlineInvoiceUrl(tenant.tenantId, params.inv)
        } catch(error) {
            return this.normalizeErrorResponse(error)
        }

        return XeroUtils.buildResponse(true, invoiceUrl.body.onlineInvoices[0].onlineInvoiceUrl)
    }

    async paymentComplete()
    {
        const { transactionId, sc } = await useBody(this.event)
        // @ts-ignore
        const storageItem: TransactionStoreItem = await this.event.req.storage.storage.getItem(transactionId)

        if (!storageItem) {
            this.event.res.statusCode = 404
            return XeroUtils.buildResponse(false, `Vendor TX Code ${transactionId} Not Found`)
        }

        const { paymentPostData, response, threeDSResponse } = storageItem

        const lastStatus = threeDSResponse || response

        if (lastStatus.status !== 'Ok') {
            this.event.res.statusCode = 400
            return XeroUtils.buildResponse(false, `The response status for ${transactionId} is not Ok`)
        }

        let tenant
        try {
            tenant = await this.fetchTenant(sc)
        } catch (error) {
            return this.normalizeErrorResponse(error)
        }

        const paymentAccountId = await this.resolvePaymentAccounts(tenant.tenantId, lastStatus.paymentMethod.card.cardType)
        const paymentInfo = {
            paymentAmountRemaining: lastStatus.amount.saleAmount / 100
        }

        try {
            for (const invoice of paymentPostData.basket.selectedInvoices) {
                await this.markInvoicePaid(tenant.tenantId, paymentAccountId, paymentInfo, invoice)
                await this.createAndPayFees(tenant.tenantId, paymentAccountId, paymentInfo, invoice, paymentPostData.basket.contact.contactID)
            }
        } catch (error) {
            return this.normalizeErrorResponse(error)
        }

        return XeroUtils.buildResponse(true, 'Ok')
    }

    private normalizeErrorResponse(error)
    {
        if ((error instanceof FetchError)) {
            this.event.res.statusCode = error.response.status
            return XeroUtils.buildResponse(false, error.data || error.message)
        }
        this.event.res.statusCode = 500
        return XeroUtils.buildResponse(false, error.data || error.message)
    }

    private static buildResponse(success, message = null, obj = {}) {
        return {
            success,
            message,
            ...obj
        }
    }

    private async resolvePaymentAccounts(tenantId, cardType)
    {
        const paymentAccounts = {
            paypal: null,
            default: null
        }
        const xeroPaymentAccounts = await this.fetchPaymentAccounts(tenantId)
        for(const account of xeroPaymentAccounts) {
            if (account.code === runtimeConfig.XERO_PAYPAL_ACCOUNT_CODE) {
                paymentAccounts.paypal = account.accountID
            }
            if (account.code === runtimeConfig.XERO_OPAYO_ACCOUNT_CODE) {
                paymentAccounts.default = account.accountID
            }
        }
        return paymentAccounts?.[cardType.toLowerCase()] || paymentAccounts.default
    }

    private async markInvoicePaid(tenantId, paymentAccountId, paymentInfo, invoice)
    {
        const payAmount = Math.min(paymentInfo.paymentAmountRemaining, invoice.amountDue)
        const date = new Date().toISOString().slice(0, 19).replace('T', ' ');

        let response
        try {
            const newPayment: Payment = {
                invoice: {
                    invoiceID: invoice.invoiceID
                },
                account: {
                    accountID: paymentAccountId
                },
                date,
                amount: payAmount
            }
            response = await this.xero.accountingApi.createPayment(tenantId, newPayment)
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }

        for (const payment of response.body.payments) {
            paymentInfo.paymentAmountRemaining-=payment.amount
        }
    }

    private async createAndPayFees(tenantId, paymentAccountId, paymentInfo, invoice, contactID)
    {
        const date = new Date().toISOString().slice(0, 19).replace('T', ' ');
        const fees = calculateFees(invoice.amountDue)

        if (fees === 0) {
            return
        }

        const invoices = new Invoices()
        invoices.invoices = [
            {
                type: TypeEnum.ACCREC,
                contact: {
                    contactID
                },
                date,
                dueDate: date,
                lineAmountTypes: LineAmountTypes.Inclusive,
                status: StatusEnum.AUTHORISED,
                lineItems: [
                    {
                        description: `Online payment fee - Invoice ${invoice.invoiceNumber}`,
                        quantity: 1,
                        unitAmount: fees,
                        accountCode: runtimeConfig.XERO_PAYMENT_FEE_ACCOUNT_CODE
                    }
                ]
            }
        ]

        let response
        try {
            response = await this.xero.accountingApi.createInvoices(tenantId, invoices)
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }


        for (const feeInvoice of response.body.invoices) {
            await this.markInvoicePaid(tenantId, paymentAccountId, paymentInfo, feeInvoice)
        }
    }

    private static stripInvoiceKeys(invoice: Invoice)
    {
        const keys = ['type', 'date', 'dueDate', 'status', 'lineAmountTypes', 'lineItems', 'subTotal', 'totalTax', 'total', 'totalDiscount', 'invoiceNumber', 'reference', 'currencyCode', 'amountDue', 'amountPaid', 'updatedDateUTC', 'invoiceID']
        return keys.reduce((cur, key) => { return Object.assign(cur, { [key]: invoice[key] })}, {})
    }

    private async fetchBankAccounts(tenantId: string)
    {
        try {
            const data = await this.xero.accountingApi.getAccounts(tenantId, null, 'Type=="BANK"&&EnablePaymentsToAccount==false&&!Name.Contains("Savings")&&!Name.Contains("Cash")&&!Name.Contains("PayPal")&&Status=="ACTIVE"&&!BankAccountNumber.StartsWith("100000")')
            return data.body.accounts
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
    }

    private async fetchPaymentAccounts(tenantId: string)
    {
        try {
            const data = await this.xero.accountingApi.getAccounts(tenantId, null, `Code="${runtimeConfig.XERO_PAYPAL_ACCOUNT_CODE}"||Code="${runtimeConfig.XERO_OPAYO_ACCOUNT_CODE}"`)
            return data.body.accounts
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
    }

    private async fetchOnlineInvoiceUrl(xeroTenantId: string, invoiceId: string)
    {
        try {
            return await this.xero.accountingApi.getOnlineInvoice(xeroTenantId, invoiceId)
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
    }

    private async fetchInvoicesByContact(tenantId: string, contactId: string)
    {
        try {
            const data = await this.xero.accountingApi.getInvoices(tenantId, null, 'Type=="ACCREC"', null, null, null, [contactId], ['AUTHORISED'])
            return data.body.invoices
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
    }

    private async fetchInvoice(tenantId: string, invoice: string)
    {
        try {
            const data = await this.xero.accountingApi.getInvoice(tenantId, invoice)
            return data.body.invoices[0]
        } catch (error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
    }

    private async fetchTenant(sc: string)
    {
        try {
            await this.xero.updateTenants()
        } catch(error) {
            XeroUtils.handleXeroErrorResponse(error)
        }
        for (const tenant of this.xero.tenants) {
            const { orgData: { shortCode } } = tenant
            if (shortCode === sc) {
                return tenant
            }
        }

        this.event.res.statusCode = 403
        this.event.res.write(`Access has not been granted to the organisation with short code ${sc}`)
        this.event.res.end()
    }

    private static handleXeroErrorResponse(error)
    {
        if ((error instanceof FetchError)) {
            throw error
        }
        throw new Error(error.data || error.message || JSON.stringify(error.response.body))
    }
}
