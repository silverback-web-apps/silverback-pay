import ngrok from "ngrok";
import type { Ngrok } from "ngrok";
import chalk from "chalk";
import { defineNuxtModule, addTemplate } from "@nuxt/kit";
import consola from "consola";

// https://github.com/bubenshchykov/ngrok
export type ModuleOptions = Partial<Ngrok.Options>;

const CONFIG_KEY = "ngrok";

export default defineNuxtModule({
  meta: {
    // Usually  npm package name of your module
    name: "ngrok",
    // The key in `nuxt.config` that holds your module options
    configKey: CONFIG_KEY,
    // Compatibility constraints
    compatibility: {
      nuxt: "^3.0.0",
    },
  },
  // Default configuration options for your module
  defaults: {
    authtoken: process.env.NGROK_TOKEN,
    auth: process.env.NGROK_AUTH,
  },
  hooks: {},
  setup: async (moduleOptions: ModuleOptions, nuxt) => {
    const CREATE_NGROK_TEMPLATE = (url) => {
      addTemplate({
        filename: 'ngrok.mjs',
        write: true,
        getContents: () => `export const url = ${JSON.stringify(url)}`,
      });
    }

    // Don't start NGROK in production mode
    if (nuxt.options.dev === false) {
      CREATE_NGROK_TEMPLATE(null)
      return;
    }
    if (!moduleOptions.auth) {
      // eslint-disable-next-line no-console
      consola.warn(
        "[ngrok] Dev server exposed to internet without password protection! Consider using `ngrok.auth` options"
      );
    }

    let url: string;

    nuxt.hook("listen", async (_server: any, { port }: { port: number }) => {
      if (moduleOptions.authToken) {
        await ngrok.authtoken(moduleOptions.authToken);
      }

      url = await ngrok.connect({
        ...moduleOptions,
        addr: port,
      } as Ngrok.Options);

      CREATE_NGROK_TEMPLATE(url)

      consola.success(chalk.underline.green(`ngrok connected: ${url}`));
    });

    // Disconnect ngrok connection when closing nuxt
    nuxt.hook("close", () => {
      if (url) {
        ngrok.disconnect();
        consola.success(chalk.underline.yellow("ngrok disconnected"));
        url = null;
      }
    });
  },
});
