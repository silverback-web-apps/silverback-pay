import { defineNuxtConfig } from "nuxt";
import svgLoader from "vite-svg-loader";

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    privateRuntimeConfig: {
        XERO_CLIENT_ID: process.env.XERO_CLIENT_ID || "",
        XERO_CLIENT_SECRET: process.env.XERO_CLIENT_SECRET || "",
        XERO_SCOPES: process.env.XERO_SCOPES || "",
        XERO_PAYPAL_ACCOUNT_CODE: process.env.XERO_PAYPAL_ACCOUNT_CODE || "",
        XERO_OPAYO_ACCOUNT_CODE: process.env.XERO_OPAYO_ACCOUNT_CODE || "",
        XERO_PAYMENT_FEE_ACCOUNT_CODE: process.env.XERO_PAYMENT_FEE_ACCOUNT_CODE || "",
        OPAYO_INTEGRATION_KEY: process.env.OPAYO_INTEGRATION_KEY || "",
        OPAYO_INTEGRATION_PASSWORD: process.env.OPAYO_INTEGRATION_PASSWORD || "",
        OPAYO_VENDOR: process.env.OPAYO_VENDOR || "sandbox",
        REDIS_URL: process.env.REDIS_URL || "redis",
    },
    publicRuntimeConfig: {
        APP_HOST: process.env.APP_HOST,
        OPAYO_HOST: process.env.OPAYO_HOST || "https://sandbox.opayo.eu.elavon.com/api/v1"
    },
    buildModules: ["~/modules/google-fonts"],
    modules: ["~/modules/ngrok"],
    googleFonts: {
        display: "swap",
        prefetch: true,
        preconnect: true,
        preload: true,
        families: {
            Montserrat: {
                wght: [300, 400, 500, 600],
                ital: [100],
            },
        },
    },
    css: ["@/assets/sass/main.sass"],
    vite: {
        resolve: {
            alias: {
                "~bulma": "bulma",
            },
        },
        // @ts-ignore
        plugins: [svgLoader()],
        css: {
            preprocessorOptions: {
                sass: {
                    additionalData: '@import "@/assets/sass/_vars/all.sass"\n',
                },
            },
        },
        server: {
            hmr: {
                // protocol: "ws",
                // host: "*",
                // port: 443,
                // clientPort: 443,
                // path: '/_nuxt/'
            },
        },
    },
    ngrok: {
        authToken: process.env.NGROK_AUTHTOKEN,
        region: "eu",
        port: 3000,
    },
});
