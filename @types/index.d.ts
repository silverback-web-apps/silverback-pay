import ServerStorage from "~/utils/server-storage";
import { NuxtApp } from "@nuxt/schema";
import XeroUtils from '~/utils/xero-utils';
import OpayoUtils from '~/utils/opayo-utils';

declare module '@nuxt/schema' {
  interface RuntimeConfig {
    public: {}
  }
}

type JsonResponseFn = (data: any) => void;

declare module "http" {
  interface IncomingMessage {
    storage?: ServerStorage;
    xero?: XeroUtils;
    opayo?: OpayoUtils;
    nuxtApp?: NuxtApp;
  }
  interface ServerResponse {
    jsonResponse?: JsonResponseFn
  }
}

export {};
