module.exports = {
  extends: [
    "plugin:vue/vue3-recommended",
    "@vue/eslint-config-typescript/recommended",
    "@vue/eslint-config-prettier",
  ],
  root: true,
  env: {
    node: true,
  },
  rules: {
    "@typescript-eslint/no-unused-vars": "error",
    "no-unused-vars": "error",
    "vue/script-setup-uses-vars": "error",
  },
};
