ARG NODE_VERSION=14.19.0

FROM node:${NODE_VERSION} as builder

WORKDIR /app

COPY . .

RUN yarn install \
  --prefer-offline \
  --frozen-lockfile \
  --non-interactive \
  --production=false

RUN yarn build

RUN rm -rf node_modules && \
  NODE_ENV=production yarn install \
  --prefer-offline \
  --pure-lockfile \
  --non-interactive \
  --production=true

FROM node:${NODE_VERSION} as app

WORKDIR /app

COPY --from=builder /app  .

ENV HOST 0.0.0.0
EXPOSE 3000

COPY start.sh /usr/local/bin/start
RUN chmod +x /usr/local/bin/start
CMD ["start"]
